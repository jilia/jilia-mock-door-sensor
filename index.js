var Device = require('zetta-device');
var util = require('util');

var configDefaults = {
  quiet: false,
  randomEvents: true,
  changeIntervalMin: 5000,
  changeIntervalMax: 20000
};

var sensorStates = {
  closed: 'Closed',
  open: 'Open'
};

var newStatusToggle = {};
newStatusToggle[sensorStates.closed] = sensorStates.open;
newStatusToggle[sensorStates.open] = sensorStates.closed;

function randomBound(low,high) {
  return Math.floor(Math.random() * (high-low+1) + low);
}

// Name defaults to 'door' if not provided
var DoorSensor = module.exports = function(name, options) {
  Device.call(this);
  this.name = name || 'door';

  var config = getConfig(options);
  if(config.quiet) {
    // quiet: `log` => `noop`
    log = function(){}
  }

  this.status = sensorStates.closed;
  this.mock_RandomEvents = config.randomEvents;

  // some time later, toggle the state
  var nextTick = function() {
    return randomBound(config.changeIntervalMin, config.changeIntervalMax)
  };
  this._tick = this._tick.bind(this, nextTick);
  this._tick();
}

util.inherits(DoorSensor, Device);

DoorSensor.prototype.init = function(config) {
  config.name(this.name).state('ready')
    .type('contact')
    .monitor('status')
    .map('mock_SetRandomEvents', this.setRandomEvents, [
      {name:'randomEvents', type:'radio', value:[
        {value:'on'}, {value:'off'}
      ]}
    ])
    .map('mock_TriggerOpen', this.openDoor)
    .map('mock_TriggerClose', this.closeDoor)
    .map('mock_TriggerStatusToggle', this.toggleDoor)
    .when('ready', {
      allow: [
        'mock_SetRandomEvents',
        'mock_TriggerOpen',
        'mock_TriggerClose',
        'mock_TriggerStateToggle'
      ]
    });
}

DoorSensor.prototype.setRandomEvents = function(state, cb) {
  var on = state === 'on';
  this.mock_RandomEvents = on;
  cb();
};

DoorSensor.prototype.openDoor = function(cb) {
  this.status = sensorStates.open;
  cb();
};

DoorSensor.prototype.closeDoor = function(cb) {
  this.status = sensorStates.closed;
  cb();
};

DoorSensor.prototype.toggleDoor = function(cb) {
  this.status = newStatusToggle[this.status];
  cb();
};

DoorSensor.prototype._tick = function(nextTick) {
  setTimeout(function(){
    if(this.mock_RandomEvents) {
      this.status = newStatusToggle[this.status];
    }
    this._tick();
  }.bind(this), nextTick());
};

function log(message) {
  console.log(message);
}

function getConfig(options) {
  var result = {};

  options = options || {};
  if(typeof options === 'string') {
    try { options = JSON.parse(options); }
    catch(err) { console.log(err); }
  }

  Object.keys(configDefaults).forEach(function(prop){
    var override = options.hasOwnProperty(prop);
    result[prop] = override ? options[prop] : configDefaults[prop];
  });

  return result;
}